// console.log("Hello World");

const num = 2;
const getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`);


/* 

// Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// Hello Juan Dela Cruz! It's nice to meet you
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

*/
const address = ["258 Washington Ave NW", "California 90011"];

const [address1,address2] = address;
console.log(`I live at ${address1}, ${address2}`);
/* 
let squareValues = address;
console.log(`I live at ` +squareValues);
 */
/* 

const numbers = [1, 2, 3, 4, 5];

let squareValues = numbers.map((number) => number**2);
console.log(squareValues);

*/


/* 
console.log(`I live at ${address.addressNumber} ${address.addressStreet}, ${address.addressCountry} ${address.addressPostCode}`);
 */


const animal = {
	animalName: "Lolong",
	animalType: "saltwater crocodile",
	animalWeight: 1075,
    animalMeasure: "20 ft 3 in"
}

console.log(`${animal.animalName} was a ${animal.animalType}. He weighed at ${animal.animalWeight} kgs with a measurement of ${animal.animalMeasure}.`);





const numbers = [1, 2, 3, 4, 5];
/* 
let squareValues = numbers.map((number) => number**2);
console.log(squareValues);
 */
numbers.forEach((number) => 
{
    console.log(`${number}`);
}
);



let i = 0;
let reduceNumber = numbers.reduce((number, i) => i=number+i /* ++i + number */);
console.log(reduceNumber);

class Dog{
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

let myDog = new Dog;
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";

console.log(myDog);
/* 
Instructions for s24 Activity:



12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.

*/